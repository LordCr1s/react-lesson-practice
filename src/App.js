import React, { Component } from "react";
import Counters from "./components/counters";
import NavBar from "./components/navbar";
import "./App.css";

class App extends Component {

  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
      { id: 5, value: 0 }
    ], 
  };

 

  deleteCounter = counterId => {
    const counters = this.state.counters.filter(c => c.id !== counterId);
    this.setState({ counters });
  };

  resetCounters = () => {
    const counters = this.state.counters.map(c => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  incrementCount = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value += 1;
    this.setState({ counters });
  };
  
  render() {
    return (
      <React.Fragment>
        <NavBar counters={this.state.counters}/>
        <main className="container" style={{ "margin-top":30 }}>
          <Counters 
          counters={this.state.counters}
          deleteCounter={this.deleteCounter} 
          incrementCount={this.incrementCount}
          resetCounters={this.resetCounters}/>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
