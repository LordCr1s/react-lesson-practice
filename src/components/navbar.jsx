import React, { Component } from "react";

class NavBar extends Component {
  state = {};

  render() {
      const { counters } = this.props;
    return (
      <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
          Navbar
        </a>
        <span style={{ fontSize: 15 }} className="badge m-2 badge-success">
          {counters.filter(c => c.value > 0).length} Objects
        </span>
      </nav>
    );
  }
}

export default NavBar;
