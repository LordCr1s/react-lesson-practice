import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    return (
      <div>
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={this.props.resetCounters}
        >
          Reset
        </button>
        {this.props.counters.map(counter => (
          <Counter
            key={counter.id}
            counter={counter}
            onDelete={this.props.deleteCounter}
            incrementCount={this.props.incrementCount}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
